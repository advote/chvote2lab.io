# GitLab pages

The Gitlab pages are built using the Jekyll static site generator.

They are based on the skinny bones template.

## Build an run locally

### Prerequisites
Please read https://jekyllrb.com/docs/ and follow steps 1 to 6.

Note: installing Ruby on an environment being behind a corporate proxy with a custom CA can be
quite tricky.

### Build and run

Run the following command line:

``bundler exec jekyll serve``
