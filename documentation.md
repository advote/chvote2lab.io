---
layout: article
title: Documentation
excerpt: CHVote functional and technical documentation
tags: CHVote, evoting, opensource, release, documentation, requirements, architecture, design, release note, security, specifications
permalink: /doc/
image:
  feature: documents-banner.png
---

{% include toc.html %}

> Please note: the technical documentation is in English whereas the functional documentation is in French.

## Functional documentation

### General functional documentation

<div class="documents cards">

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/ReleaseNote/CHVote_Product_release_notes.pdf?inline=false"
   title="Release notes (EN/FR)"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/Vision/Vision-v1.3.pdf?inline=false"
   title="Vision"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/Scrutins%20supportés/TypesDeScrutinsSupportés.pdf?inline=false"
   title="Scrutins supportés"
%}

</div>

### Detailed functional requirements
<div class="documents cards">

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/VR/SFD-Site%20de%20vote.pdf?inline=false"
   title="Spécifications fonctionnelles Site de vote"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/PACT/SIDP-SFD-PACT.pdf?inline=false"
   title="Spécifications fonctionnelles PACT"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/VRUM/SFD-VRUM.pdf?inline=false"
   title="Spécifications fonctionnelles VRUM"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/U-Print/SIDP-SFD-U-PRINT.pdf?inline=false"
   title="Spécifications fonctionnelles U-Print"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/U-Count/SIDP-SFD-U-Count.pdf?inline=false"
   title="Spécifications fonctionnelles U-Count"
%}

</div>

#### Spécifications fonctionnelles Backoffice

<div class="documents cards">

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Tableau%20de%20bord%20des%20opérations.pdf?inline=false"
   title="Tableau de bord des opérations"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Page%20de%20pilotage.pdf?inline=false"
   title="Page de pilotage"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Paramétrage-Opération.pdf?inline=false"
   title="Paramétrage opération"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Paramétrage-Jalons-Opérationnels.pdf?inline=false"
   title="Paramétrage jalons opérationnels"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Importer%20les%20référentiels%20de%20l'opération.pdf?inline=false"
   title="Importer les référentiels de l'opération"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Importer%20le%20registre%20électoral.pdf?inline=false"
   title="Importer le registre électoral"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Paramétrer%20le%20site%20de%20vote.pdf?inline=false"
   title="Paramétrer le site de vote"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Paramétrage-Importer-Documentation.pdf?inline=false"
   title="Importer la documentation"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Paramétrage-Importer-Documentation-Scrutin.pdf?inline=false"
   title="Importer la documentation pour un scrutin"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Paramétrer%20l'affichage%20d'une%20élection.pdf?inline=false"
   title="Paramétrer l'affichage d'une élection"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-D%C3%A9finir%20la%20configuration%20imprimeur.pdf?inline=false"
   title="Définir la configuration imprimeur"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Définir%20les%20cartes%20de%20vote%20de%20test%20et%20de%20contrôle.pdf?inline=false"
   title="Définir les cartes de vote de test et de contrôle"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Inviter%20une%20entité%20de%20gestion.pdf?inline=false"
   title="Inviter une entité de gestion"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Module%20d'administration.pdf?inline=false"
   title="Module d'administration"
%}
{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Tableau%20de%20bord%20Imprimeur.pdf?inline=false"
   title="Tableau de bord Imprimeur"
%}

</div>

<hr/>

## Design documentation

### Architecture

<div class="documents cards">

{% include doc.html
   url="documentation/chvote-docs/builds/artifacts/master/raw/design/target/generated-docs/pdf/architecture/system-architecture.pdf?job=artifact:design-docs"
   title="System architecture"
%}

</div>

### Applications design

<div class="documents cards">

{% include doc.html
   url="protocol-core/chvote-protocol/-/jobs/artifacts/master/raw/protocol-docs/target/generated-docs/pdf/protocol-application-design.pdf?job=build:docs"
   title="Protocol core"
%}

{% include doc.html
   url="web-apps/chvote-pact/builds/artifacts/master/raw/docs/target/generated-docs/pdf/pact-application-design.pdf?job=artifact:docs"
   title="PACT"
%}

{% include doc.html
   url="web-apps/chvote-receiver/builds/artifacts/master/raw/vote-receiver-design-docs/target/generated-docs/pdf/vote-receiver-application-design.pdf?job=artifact:docs"
   title="Vote Receiver"
%}

{% include doc.html
   url="web-apps/chvote-vrum/builds/artifacts/master/raw/chvote-vrum-design-docs/target/generated-docs/pdf/vrum-application-design.pdf?job=artifact%3Adesign-docs"
   title="VRUM"
%}

{% include doc.html
   url="web-apps/chvote-bo/builds/artifacts/master/raw/bo-design-docs/target/generated-docs/pdf/back-office-application-design.pdf?job=artifact:docs"
   title="Backoffice"
%}

</div>


### Security

<div class="documents cards">

{% include doc.html
   url="protocol-core/chvote-protocol/builds/artifacts/master/raw/protocol-docs/target/generated-docs/pdf/control-components-threat-model.pdf?job=build:docs"
   title="Control components threat model"
%}

{% include doc.html
   url="documentation/chvote-docs/builds/artifacts/master/raw/design/target/generated-docs/pdf/security/system-threat-model.pdf?job=artifact%3Adesign-docs"
   title="System threat model (not yet complete)"
%}

</div>

### Transversal design

<div class="documents cards">

{% include doc.html
   url="documentation/chvote-docs/builds/artifacts/master/raw/design/target/generated-docs/pdf/design/protocol-related-keys-management.pdf?job=artifact%3Adesign-docs"
   title="Protocol related keys management"
%}

{% include doc.html
   url="documentation/chvote-docs/builds/artifacts/master/raw/design/target/generated-docs/pdf/design/self-test-integrity-verification-features.pdf?job=artifact%3Adesign-docs"
   title="Self test and integrity verifications"
%}

</div>

### Software factory

<div class="documents cards">

{% include doc.html
   url="documentation/chvote-docs/builds/artifacts/master/raw/design/target/generated-docs/pdf/development/continuous-integration-and-deployment.part.pdf?job=artifact%3Adesign-docs"
   title="Continuous integration and deployment"
%}

</div>

<hr/>

## System interfaces contracts

<div class="documents cards">

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/Interfaces/SIDP-Contrat_interfaces_Circonscription_Dépouillement.pdf?inline=false"
   title="Circonscriptions de dépouillement"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/Interfaces/SIDP-Contrat_interfaces_Domaine_influence.pdf?inline=false"
   title="Domaine d'influence"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/Interfaces/SIDP-Contrat_interfaces_eCH-0157_Elections.pdf?inline=false"
   title="eCH-0157 Elections"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/Interfaces/SIDP-Contrat_interfaces_eCH-0159_Votations.pdf?inline=false"
   title="eCH-0159 Votations"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/Interfaces/SIDP-Contrat_interfaces_eCH-0045_Registre_electoral.pdf?inline=false"
   title="eCH-0045 Registre électoral"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/Interfaces/SIDP-Contrat_interfaces_eCH-0110_Resultats_Votation_Election.pdf?inline=false"
   title="eCH-0110 Résultats votations et élections"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/Interfaces/SIDP-Contrat_interfaces_eCH-0222_Resultats_Votation_Election.pdf?inline=false"
   title="eCH-0222 Résultats votations et élections"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/Interfaces/SIDP-Interface_Contract_eCH-0228_Printer_File-eCH_group_version.pdf?inline=false"
   title="eCH-0228 Printer files, as should be published by the eCH group (EN)"
%}


{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/Interfaces/SIDP-Interface_Contract_eCH-0228_Printer_File-OCSIN_version.pdf?inline=false"
   title="eCH-0228 Printer files, as currently implemented into the system (EN)"
%}

</div>

<hr/>

## Development directives

<div class="documents cards">

{% include doc.html
   url="documentation/chvote-docs/blob/master/development-directives/git-workflow.md"
   title="Git workflow"
%}

{% include doc.html
   url="documentation/chvote-docs/blob/master/development-directives/coding-style-Java.md"
   title="Java coding style"
%}

{% include doc.html
   url="documentation/chvote-docs/blob/master/development-directives/coding-style-FrontEnd.md"
   title="TypeScript coding style"
%}

{% include doc.html
   url="documentation/chvote-docs/blob/master/development-directives/code-review-checklist.md"
   title="Checklist for code review"
%}

{% include doc.html
   url="documentation/chvote-docs/blob/master/development-directives/release-review-checklist.md"
   title="Checklist for release review"
%}

{% include doc.html
   url="documentation/chvote-docs/blob/master/development-directives/unit-testing-principles.md"
   title="Unit testing principles"
%}

{% include doc.html
   url="documentation/chvote-docs/builds/artifacts/master/raw/design/target/generated-docs/pdf/policy/dependencies-upgrade-policy.pdf?job=artifact%3Adesign-docs"
   title="Dependencies upgrade policy"
%}

</div>
