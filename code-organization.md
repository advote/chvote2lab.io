---
layout: article
title: Repositories organization
excerpt: CHVote source code and repositories organization in Gitlab
tags: CHVote, evoting, opensource, AGPL, release, source code, git, gitlab, repository, repositories
permalink: /code/
image:
  feature: code-banner.png
---

Please find below the git repositories composing the CHVote platform source code and documentation. 

## Protocol core
<div class="cards">

{% include repo.html
  url="protocol-core/chvote-protocol"
  title="Protocol core"
  teaser="protocol-icon.png"
  excerpt="Set of backend applications running the core of the voting protocol. It includes the Control Components and the Bulletin Board applications."
%}

{% include repo.html
  url="protocol-core/chvote-protocol-model"
  title="Protocol model"
  teaser="protocol-icon.png"
  excerpt="Defines the models for the CHVote protocol that are needed outside of the protocol itself."
%}

{% include repo.html
  url="protocol-core/chvote-protocol-client"
  title="Protocol client"
  teaser="protocol-icon.png"
  excerpt="Angular library exposing the CHVote algrotihtms to be used by the voting client."
%}

</div>

## Software as a Service applications

<div class="cards">

{% include repo.html
  url="web-apps/chvote-pact"
  title="PACT"
  teaser="pact-icon.png"
  excerpt="Web application used to validate privileged actions on the system."
%}

{% include repo.html
  url="web-apps/chvote-receiver"
  title="Vote Receiver"
  teaser="vote-receiver-icon.png"
  excerpt="Web application used by the voters to cast and confirm votes."
%}

{% include repo.html
  url="web-apps/chvote-vrum"
  title="VRUM"
  teaser="vrum-icon.png"
  excerpt="Web application used to record and check the usage of the voting rights."
%}

{% include repo.html
  url="web-apps/chvote-bo"
  title="Backoffice"
  teaser="bo-icon.png"
  excerpt="Web application used to setup elections and votations."
%}
</div>

## Standalone applications running on offline computers
<div class="cards">

{% include repo.html
  url="offline-apps/u-print"
  title="U-Print"
  teaser="uprint-icon.png"
  excerpt="Standalone application dedicated to the printers designed to generate and visualize the voting material."
%}

{% include repo.html
  url="offline-apps/u-count"
  title="U-Count"
  teaser="ucount-icon.png"
  excerpt="Standalone application dedicated to the election officers designed to decrypt the ballots and to compute the tally of the votes."
%}

{% include repo.html
  url="offline-apps/chvote-prover"
  title="CHVote Prover"
  teaser="uverify-icon.png"
  excerpt="Standalone application dedicated to the election board designed to run the universal verification checks."
%}
</div>

## Shared libraries
<div class="cards">

{% include repo.html
  url="shared-libraries/chvote-interfaces-legacy"
  title="chvote-interfaces-legacy"
  excerpt="Library providing an API to consume and generate the eCH XML files needed in the electronic voting scope.  This is the legacy version of the library still used in the CHVote components (migration to the new version was planned in the next release while the project has been stopped)."
%}

{% include repo.html
  url="shared-libraries/chvote-model-converter"
  title="chvote-model-converter"
  excerpt="Library supplying the conversions of eCH models into internal models used for the chvote-protocol and conversely."
%}

{% include repo.html
  url="shared-libraries/jackson-serializer"
  title="jackson-serializer"
  excerpt="Library for serializing & deserializing objects between JSON and Java."
%}

{% include repo.html
  url="shared-libraries/file-namer"
  title="file-namer"
  excerpt="Library providing utility classes for generating a file name following a common design pattern."
%}

{% include repo.html
  url="shared-libraries/chvote-common-theme"
  title="chvote-common-theme"
  excerpt="This project supplies the UX elements common to all CHVote Angular frontend applications."
%}
  
{% include repo.html
  url="shared-libraries/chvote-fx-common"
  title="chvote-fx-common"
  excerpt="Common library supplying shared JavaFx-related stuff for CHVote projects."
%}
    
{% include repo.html
  url="shared-libraries/chvote-crypto"
  title="chvote-crypto"
  excerpt="Shared library supplying crypto primitives to be used for the CHVote projects."
%}

{% include repo.html
  url="shared-libraries/chvote-interfaces"
  title="chvote-interfaces"
  excerpt="New version of the library providing an API to consume and generate the eCH XML files needed in the electronic voting scope. Not yet used in CHVote components."
%}
  
</div>

## Documentations
<div class="cards">

{% include repo.html
  url="documentation/chvote-docs"
  title="Technical documentation"
  teaser="documents-icon.png"
  excerpt="Common technical documentation for the CHVote projects."
%}

{% include repo.html
  url="documentation/chvote-docs-functional"
  title="Functional documentation"
  teaser="documents-icon.png"
  excerpt="Functional requirements and release notes (documents in French only)."
%}

{% include repo.html
  url="chvote2.gitlab.io"
  title="Web pages"
  teaser="pages-icon.png"
  excerpt="The source code of the gitlab.io web pages."
%}
</div>

## Infrastructure
<div class="cards">

{% include repo.html
  url="infra"
  title="Docker images"
  teaser="infra-icon.png"
  excerpt="Images used to build and run the applications."
%}

</div>
