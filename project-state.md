---
layout: article
title: Project state
permalink: /state/
image:
  feature: roadmap-banner.png
---

## The CHVote 2.0 Project

The CHVote 2.0 project has been launched on the basis of the [mandate from the Grand Council of State of Geneva](https://ge.ch/grandconseil/data/texte/PL11867.pdf).

The main objectives of the project were to:

* Create a new eVoting platform that would implement the highest requirement of the Federal Chancellery Ordinance on
Electronic Voting, and other cantonal legal requirements to be able to drive most of the ballot types occurring in
Switzerland.
* Provide a back office tool to drive the ballots from preparation to counting, allowing complete autonomy for the
user cantons.
* Develop a platform in open source, to stimulate collaboration from international developer community and to provide
transparency on the system, to provide security and trust.

The solution has been built as a Platform as a Service, developed and operated by the State of Geneva. The project
has started in January 2017, and has been halted in December 2018 by the Geneva state Council.

## What's in

After 2 years of development, the project reached the following status:

* All modules and applications of the platform have been designed, with exception of the business activity monitoring
application.
* Development of all the designed modules and applications has been at least started.
* First version of voting protocol has been fully implemented according to the [specification in version 1.4.1](https://eprint.iacr.org/eprint-bin/getfile.pl?entry=2017/325&version=20180502:092201&file=325.pdf).
* 2 simple vote types have been completely implemented from ballot preparation to counting; implementation of other
ballot types (vote and election) has been started (maturity status depending on ballot type). The [list of supported
ballot types](https://gitlab.com/chvote2/documentation/chvote-docs-functional/raw/master/Scrutins%20support%C3%A9s/TypesDeScrutinsSupport%C3%A9s.pdf?inline=false)
can be found in the published documentation.
* The back office tool implements the [workflow described in the published documentation](https://gitlab.com/chvote2/documentation/chvote-docs-functional/raw/master/Vision/);
this workflow is based on the 15+ years experience and the practices of the user cantons of CHVote.
* [eCH standards](https://www.ech.ch/) in latest version have been implemented for input and output interfaces (IN: eCH-0007, eCH-0045,
eCH-0157, eCH-0159 ; OUT: eCH-0110, eCH-0222, eCH-0228).
* A proof verifier, with basic features has been developed (by [DemTech](https://demtech.dk/), an independent academic
third party).

## What's not in

The following features were in the product backlog when the project has been stopped and were to be implemented before 
the first production release:

* Full integrated and open source IAM solution to manage and control access for cantonal voting officers.
* [Version 2.0 of voting protocol](https://eprint.iacr.org/2017/325) has been completely specified but not yet 
implemented; this version includes:
  * Support for advanced features like write-ins, tied elections.
  * Improved usability: shorter codes, aggregated verification codes (compact ballots, empty positions).
  * Mixing improvement and performance optimisation.
* Design and build the business activity monitoring application.
* Other essential features in the whole process, as documented in the release notes, including:
  * Security features (e.g. X509 signatures verification and creation, service access restrictions, input validations,
  automated security tests).
  * Support of all expected ballot types.
  * Operations features (e.g. application metrics, integrity check probes, handling of rejected messages, business
  events logging)  .
* Proof verifier tool with more user friendly interface and verbose mode.

## Reply to issues and requests

As mentioned, the project has been stopped end of 2018.

The project team has been released since then, and the infrastructure resources have been reassigned to other needs.

We are happy to reply to issues and requests, in a best effort mode. But the solution and its documentation are
published with no guarantee and support.
